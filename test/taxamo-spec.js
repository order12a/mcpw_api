'use strict';

let chakram = require('chakram');
let expect = chakram.expect;
let Config = require('config-js');
let logger = require('js-logging').colorConsole();

describe('Taxamo CRUD test samples', function () {
    let config = new Config('./config_##.js');
    let headers  = {
        "Content-Type": "application/json"
    };
    let apiUrl = config.get('api_url');
    let privateTocken = config.get('private_token');
    let transaction;
    let key;

    before('Setup base transaction', function () {
        let body = {
            "transaction": {
                "transaction_lines": [
                    {
                        "custom_id": "line1",
                        "amount": 200
                    }
                ],
                "currency_code": "EUR",
                "billing_country_code": "RU",
                "buyer_credit_card_prefix": "424242421"
            },
            "public_token": config.get('public_token')
        };
        logger.notice('Request: \n' + apiUrl + '\n' + JSON.stringify(body) + '\n' + JSON.stringify(headers));
        transaction = chakram.post(apiUrl, body, headers);

        return transaction.then(function (respObj) {
            logger.warning('Response: ' + JSON.stringify(respObj));
            key = respObj.body.transaction.key;
        });
    });

    describe('Transaction should be stored', function () {
        let resp;
        let body = {
            "transaction": {
                "transaction_lines": [
                    {
                        "custom_id": "line1",
                        "amount": 100
                    }
                ],
                "currency_code": "EUR",
                "billing_country_code": "BE",
                "buyer_credit_card_prefix": "424242424"
            },
            "public_token": config.get('public_token')
        };

        before('Create new transaction', function () {
            logger.notice('Request: ' + apiUrl + '\n' + JSON.stringify(body) + '\n' + JSON.stringify(headers));
            resp = chakram.post(apiUrl, body, headers);
            resp.then(function(response){
                logger.warning(JSON.stringify(response));
            });
        });

        it('Response code should be 200 and have content type "application/json; charset=utf-8"', function () {
            expect(resp).to.have.status(200);
            expect(resp).to.have.header("content-type", "application/json; charset=utf-8");
            return chakram.wait();
        });

        it('Should have specified amount of money', function () {
            resp.then(function () {
                expect(body.transaction.amount).to.be(100);
            });
        });
    });

    describe('Retrieve transaction', function () {
        let privateToken  = {
            "Private-Token": privateTocken
        };
        let resp;
        
        before('Retrieve transaction response', function () {
            let options = {
                headers: privateToken
            };
            let requestUrl = apiUrl + '/' + key;
            logger.notice(requestUrl + '\n' +  JSON.stringify(options));
            resp = chakram.get(requestUrl, options);
            resp.then(function(response){
                logger.warning('Response: ' + JSON.stringify(response));
            });
        });

        it('Transaction should be retrieved', function () {
            expect(resp).to.have.status(200);
            expect(resp).to.have.header("content-type", "application/json; charset=utf-8");
            return chakram.wait();
        });

        it('Test status should be provided', function () {
            return expect(resp).to.have.json('transaction.test', true);
        });

    });

    describe('Update transaction', function () {
        let body = {
            "transaction": {
                "custom_id": "custom_id_121441",
                    "order_date": "2013-11-14",
                    "currency_code": "EUR",
                    "billing_country_code": "IE",
                    "buyer_credit_card_prefix": "424242424",
                    "buyer_tax_number": "IE5251981413X",
                    "tax_country_code": "IE",
                    "tax_deducted": true,
                    "buyer_ip": "127.0.0.1",
                    "invoice_date": "2014-11-15",
                    "invoice_place": "Geneva, Switzerland",
                    "invoice_number": "511414/2013",
                    "invoice_address": {
                    "street_name": "Langford St",
                        "building_number": "31",
                        "city": "Killorglin",
                        "region": "Kerry"
                },
                "transaction_lines": [
                    {
                        "custom_id": "line1",
                        "line_key": "e_43E3wlNnpJ0tpj",
                        "product_type": "e-service",
                        "quantity": 1,
                        "amount": 100,
                        "description": "hosting",
                        "product_code": "hosting_1"
                    }
                ]
            }
        };
        let options = {
            headers:{
                'Private-Token': privateTocken,
                'Content-Type': 'application/json'
            }
        };
        let resp;
        
        before('Initial update', function () {
            let requestUrl = apiUrl + '/' + key;
            logger.notice(requestUrl + '\n' + JSON.stringify(body) + '\n' + JSON.stringify(options));
            resp = chakram.put(requestUrl, body, options);
            resp.then(function(response){
                logger.warning('Response: ' + JSON.stringify(response));
            });
        });
        
        it('Billing country should be updated', function () {
            return expect(resp).to.have.json('transaction.billing_country_code', 'IE')
        });

        it('Buyer ip and invoice place should be updated', function () {
            resp.then(function () {
                expect(body.transaction.buyer_ip).to.be('127.0.0.1');
                expect(body.transaction.invoice_place).to.be('Geneva, Switzerland');
            });
        });
    });

    describe('Delete transaction', function () {

        it('Transaction should be deleted successfully', function () {
            let options = {
                headers: {
                    "Private-Token": privateTocken
                }
            };
            let requestUrl = apiUrl + '/' + key;
            logger.notice(requestUrl + '\n' + JSON.stringify(options));
            let resp = chakram.delete(requestUrl, '{}', options);
            resp.then(function(response){
                logger.warning('Response: ' + JSON.stringify(response));
            });
            return expect(resp).to.have.json('success', true);
        });
    });
});