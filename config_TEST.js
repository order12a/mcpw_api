'use strict';

module.exports = {
    public_token: "SamplePublicTestKey1",
    private_token: "SamplePrivateTestKey1",
    api_url: "https://api.taxamo.com/api/v1/transactions"
};